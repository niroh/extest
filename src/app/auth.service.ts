import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { User } from './interfaces/user';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public afAuth:AngularFireAuth,
              public router:Router) {
            this.user = this.afAuth.authState}
   user:Observable <any | null>
   public err:any;  
   

   signup(email:string, password:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,password)
        .then(res =>{
                    console.log('Succesful sign up',res);
                    this.router.navigate(['/welcome']);
                    })
                    .catch(
                    error => {this.err = error})  
  }

  logout(){
    this.afAuth.auth.signOut().then(res => console.log('Signed Out'));
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
           res =>  
            {
              console.log('Succesful Login',res);
              this.router.navigate(['/welcome']);
            })
        .catch (
          error => {this.err = error
          }) 
   }
  }
  
