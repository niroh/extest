import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(public http:HttpClient,
              public db:AngularFirestore,
              public router:Router) { }

  getArticles(): Observable<any>{
  return this.db.collection('articles').valueChanges(({idField:'id'}));      
  }

  saveArticle(body:string,aiCat:string, manCat:string){
    const article = {body:body, aiCat:aiCat, manCat:manCat}
     this.db.collection('articles').add(article);
     this.router.navigate(['/articles'])
  }

}
