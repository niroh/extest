import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  constructor(private http:HttpClient) { }
  
  private url = "https://ecxfwltne4.execute-api.us-east-1.amazonaws.com/beta150";

  public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'};
  public doc:string;

  classify():Observable<any>{
    let json = {
      "articles": [
        {"text": this.doc}
      ]
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        let final = res.body.replace('[', '');
        final = final.replace(']', '');
        return final;
      })
    )
  }


}
