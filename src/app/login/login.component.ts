import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { User } from '../interfaces/user';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public authservice:AuthService,
                  private router:Router,
                  private activatedroute:ActivatedRoute) { }
  email:string;
  password:string;
  userData$: Observable<any>;
 

  onSubmit(){
        this.authservice.login(this.email, this.password)
    }

  
  ngOnInit() {
  }

}
