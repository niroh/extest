import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';

@Component({
  selector: 'app-docform',
  templateUrl: './docform.component.html',
  styleUrls: ['./docform.component.css']
})
export class DocformComponent implements OnInit {

  constructor(private classifyservice:ClassifyService,
              private router:Router) { }
              
  text:string;

  onSubmit(){
    this.classifyservice.doc = this.text;
    this.router.navigate(['/classified']);
  }

  ngOnInit() {
  }

}
