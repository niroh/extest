import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ArticleService } from '../article.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  constructor(public authservice:AuthService,
              public router:Router,
              public activatedroute:ActivatedRoute,
              public articleservice:ArticleService) { }
  email:string;
  password:string;

  articles$:Observable<any>;

  ngOnInit() {
    this.articles$ = this.articleservice.getArticles();
  }

}
