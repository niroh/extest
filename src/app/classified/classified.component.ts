import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { ArticleService } from '../article.service';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  constructor(public classifyservice:ClassifyService,
              public articleservice:ArticleService) { }

  category:string = "Loading...";

  categorytypes:object[]=[{name: 'business'}, {name: 'entertainment'}, {name: 'politics'}, {name: 'sport'}, {name: 'tech'}];
  
  body:string;
  aiCat:string;
  manCat:string;

  ngOnInit() {
  this.classifyservice.classify().subscribe(
      res => {
       this.category = this.classifyservice.categories[res];
       this.body = this.classifyservice.doc
       }
    )
  
  }
  onSubmit(){
    this.articleservice.saveArticle(this.body, this.category, this.manCat)
    
  }
            

}
