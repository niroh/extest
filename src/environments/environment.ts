// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA17Fi6MDD-BtIC_NQ0kZMrS3SoZ-1_enE",
    authDomain: "extest-22c82.firebaseapp.com",
    databaseURL: "https://extest-22c82.firebaseio.com",
    projectId: "extest-22c82",
    storageBucket: "extest-22c82.appspot.com",
    messagingSenderId: "90691632297",
    appId: "1:90691632297:web:8dad2444c42f18eb8abdb8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
